## weeb_RMX2020-userdebug 12 SP1A.210812.016 eng.root.20211022.032603 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: weeb_RMX2020-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.root.20211022.032603
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP1A.210812.016.A1/7796139:user/release-keys
- OTA version: 
- Branch: weeb_RMX2020-userdebug-12-SP1A.210812.016-eng.root.20211022.032603-release-keys
- Repo: realme_rmx2020_dump_11257


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
